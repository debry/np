// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of np.
//
// np is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// np is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with np.  If not, see <http://www.gnu.org/licenses/>.


#include<vector>
#include<iostream>
using namespace std;

namespace myparam
{
#include "np.hxx"
  NAMED_PARAMETER(size, int, 0);
  NAMED_PARAMETER(ratio, double, 4.5);
  NAMED_PARAMETER(verbose, bool, false);
  NAMED_PARAMETER(output, string, "path");
  NAMED_PARAMETER(myvector, std::vector<int>, std::vector<int>(10, 4));
}


void function(myparam::base *opt1 = NULL, myparam::base *opt2 = NULL,
              myparam::base *opt3 = NULL, myparam::base *opt4 = NULL)
{
  myparam::vector opt = {opt1, opt2, opt3, opt4};

  int size(0);
  double ratio(0.0);
  bool verbose(false);
  string output("");

  opt.get("size", size);
  opt.get("ratio", ratio);
  opt.get("verbose", verbose);
  opt.get("output", output);

  cout << "size = " << size << endl;
  cout << "ratio = " << ratio << endl;
  cout << "verbose = " << (verbose ? "yes" : "no") << endl;
  cout << "output = \"" << output << "\"" << endl;

  cout << string(50, '-') << endl;

  int *size_ptr = opt.getptr<int>("size");
  double *ratio_ptr = opt.getptr<double>("ratio");
  bool *verbose_ptr = opt.getptr<bool>("verbose");
  string *output_ptr = opt.getptr<string>("output");

  if (size_ptr != NULL) cout << "size = " << *size_ptr  << endl;
  if (ratio_ptr != NULL) cout << "ratio = " << *ratio_ptr  << endl;
  if (verbose_ptr != NULL) cout << "verbose = " << (*verbose_ptr ? "yes" : "no") << endl;
  if (output_ptr != NULL) cout << "output = \"" << *output_ptr << "\"" << endl;
}


int main(void)
{
  cout << string(50, '=') << endl;
  function(myparam::size = 45, myparam::verbose = false,
           myparam::ratio = 3.14, myparam::output = "azazazaz");

  cout << string(50, '=') << endl;
  function(myparam::verbose = true, myparam::output = "qsqsqsqs");

  cout << string(50, '=') << endl;
  function(myparam::ratio = 2.71, myparam::size = 12);

  cout << string(50, '=') << endl;
  function(&myparam::verbose, &myparam::size, &myparam::output);

  cout << string(50, '=') << endl;
  function(myparam::size = 34, myparam::output = "fgfgfgf",
           myparam::verbose = true, myparam::ratio = 0.214);

  cout << string(50, '=') << endl;
  function(myparam::verbose = true, myparam::size = 67);

  cout << string(50, '=') << endl;
  function(myparam::verbose = false);

  cout << string(50, '=') << endl;
  function(&myparam::verbose, &myparam::size, &myparam::output, &myparam::ratio);

  cout << string(50, '=') << endl;
  function(&myparam::verbose, &myparam::size, &myparam::output, &myparam::ratio);

  cout << string(50, '=') << endl;
}
