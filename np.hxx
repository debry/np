// Copyright (C) 2015 Edouard Debry
// Author(s) : Edouard Debry
//
// This file is part of np.
//
// np is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// np is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with np.  If not, see <http://www.gnu.org/licenses/>.

#ifndef NP_FILE_NP_HXX

#include<string>
#include<vector>
using namespace std;


class base
{
public:

  string name_;
  void *value_ptr_;

public:

  base(string name, void *value_ptr) : name_(name), value_ptr_(value_ptr) {return;}

  virtual void reset() = 0;
};


class vector : public std::vector<base*>
{
public:

  vector(initializer_list<base*> list)
  {
    for (initializer_list<base*>::iterator i = list.begin(); i != list.end(); i++)
      this->push_back(*i);
    return;
  }


  ~vector()
  {
    for (std::vector<base*>::iterator it = this->begin(); it != this->end(); it++)
      if (*it != NULL) (*it)->reset();
    return;
  }


  template<class T>
  T* getptr(const string &name)
  {
    for (std::vector<base*>::iterator it = this->begin(); it != this->end(); it++)
      if (*it != NULL)
        if ((*it)->name_ == name)
          return reinterpret_cast<T*>((*it)->value_ptr_);

    return NULL;
  }
 
 
  template<typename T>
  void get(const string &name, T &variable)
  {
    for (std::vector<base*>::iterator it = this->begin(); it != this->end(); it++)
      if ((*it) != NULL)
        if ((*it)->name_ == name)
          {
            variable = *(reinterpret_cast<T*>((*it)->value_ptr_));
            break;
          }
  }
};


#define NAMED_PARAMETER(name, type, default)        \
  class name : public base                          \
  {                                                 \
  private:                                          \
    type value_, default_;                          \
                                                    \
  public:                                           \
                                                    \
    name() : base(#name, &value_),                  \
      value_(default), default_(default) {return;}  \
                                                    \
    void reset() {value_ = default_;}               \
                                                    \
    name* operator= (type value)                    \
      {                                             \
        value_ = value;                             \
        return this;                                \
      }                                             \
  } name

#define NP_FILE_NP_HXX
#endif
